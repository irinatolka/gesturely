//
//  AppDelegate.h
//  Gesturely
//
//  Created by Jason on 04/17/13.
//  Copyright (c) 2013 Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end